# Copyright 2021 Baptiste Lambert (Blaireau) <baptiste.lambert@epfl.ch>

# This file is part of moodle-download.
#
# moodle-download is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# moodle-download is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with moodle-download.  If not, see <https://www.gnu.org/licenses/>.

import re
import requests
import yaml

from bs4 import BeautifulSoup
from pathlib import Path


DEBUG = True

BASE_DIR = Path(__file__).parent

LOGIN_URL = "https://moodle.epfl.ch/login/index.php"
CONFIG_FILE = Path('~/.config/moodle_dl/config.yaml').expanduser()
DOWNLOAD_FILE = BASE_DIR / "ressources.txt"

JOB_SEPARATOR = " "
COUNTER_FILE = ".counter"

CREDENTIALS = ["username", "password"]

savedprint = print
def info(*args, **kwargs):
    savedprint(*args, **kwargs, flush=True)

if not DEBUG:
    print = lambda *args, **kwargs : None

def disp(soup):
    print(soup.text)

class MoodleBot:

    def __init__(self):
        with open(CONFIG_FILE, 'r') as file:
            self.coursesData = yaml.load(file, Loader=yaml.CLoader)
        self.jobsDone = []
        self.switchLogedIn = False

    def get_ressource_url(self, page, ressourceType):
        if ressourceType == "pdf":
            if page.url.endswith("pdf"):
                return page.url
            soup = BeautifulSoup(page.content, "html.parser")
            for match in soup.find_all("a"):
                if match.attrs["href"].endswith("pdf"):
                    return match.attrs["href"]
        else:
           soup = BeautifulSoup(page.content, "html.parser")
           video = soup.find("video")
           return video.find("source")["src"]

    def download_ressource(self, url, fileName):
        res = self.session.get(url, stream=True)
        if fileName.is_file():
            raise ValueError(f"File {fileName} already exist !")
        with open(fileName, "wb") as file:
            file.write(res.content)

    def guess_course(self, soup):
        element = soup.find("a", **{"data-key":"coursehome"})
        courseUrl = element.attrs["href"]
        return self.get_course(courseUrl)

    def get_course(self, courseUrl):
        pageId = self.extract_id(courseUrl)
        for course in self.coursesData["courses"]:
            if course["id"] == pageId:
                return course

    def extract_id(self, courseUrl):
        match = re.match(".+id=(\d+).*", courseUrl)
        return int(match.group(1))

    def login(self):
        print("Trying to log into moodle…")
        data = {"username": self.user, "password": self.password}
        self.session = requests.Session()
        loginPage = self.session.get(LOGIN_URL)
        data["requestkey"] = loginPage.url.split("requestkey=")[1]
        self.session.post(loginPage.url, data=data)
        print("Logged in")

    def switch_login(self):
        if self.switchLogedIn:
            return
        switchLoginPage = "https://tube.switch.ch/session/new?return_to="
        switchLoginPage = self.session.get(switchLoginPage)
        payload = {"user_idp": "https://idp.epfl.ch/idp/shibboleth", "Select": "Select"}
        res = self.session.post(switchLoginPage.url, data=payload)
        payload = {"shib_idp_ls_exception.shib_idp_session_ss":"", "shib_idp_ls_success.shib_idp_session_ss":"true", "shib_idp_ls_value.shib_idp_session_ss":"", "shib_idp_ls_exception.shib_idp_persistent_ss":"", "shib_idp_ls_success.shib_idp_persistent_ss":"true", "shib_idp_ls_value.shib_idp_persistent_ss":"", "shib_idp_ls_supported":"true", "_eventId_proceed":""}
        res2 = self.session.post(res.url, data=payload)
        payload = {"j_username": self.user, "j_password": self.password,
                   "_eventId_proceed":""}
        res3 = self.session.post(res2.url, data=payload)
        payload = {"shib_idp_ls_exception.shib_idp_session_ss":"", "shib_idp_ls_success.shib_idp_session_ss":"true", "_eventId_proceed":""}
        res4 = self.session.post(res3.url, data=payload)
        soup = BeautifulSoup(res4.content, "html.parser")
        saml = soup.find("input", {"name": "SAMLResponse"})["value"]
        relay = soup.find("input", {"name": "RelayState"})["value"]
        payload = {"SAMLResponse": saml, "RelayState": relay}
        res5 = self.session.post("https://tube.switch.ch/Shibboleth.sso/SAML2/POST", data=payload)
        self.switchLogedIn = True

    def collect_ressource(self, courseUrl, ressourceUrl, destination=""):
        if "/url/" in ressourceUrl or "tube.switch.ch" in ressourceUrl:
            # video switch tube
            ressourceType = "mp4"
            self.switch_login()
        else:
            # pdfs
            ressourceType = "pdf"
        print("scrapping page…")
        page = self.session.get(ressourceUrl)
        print("parsing page…")
        ressource = self.get_ressource_url(page, ressourceType)
        if ressource is None:
            raise ValueError(f"No ressource found at {ressourceUrl}")
        course = self.get_course(courseUrl)
        if course is None:
            raise ValueError(f"No course found matching {courseUrl}")
        directory = self.download_destination / course['dir'] / destination
        filename = ressource.split("/")[-1]
        if len(filename) > 200:
            filename = f"{ressourceType}.{ressourceType}"
        destination = directory/filename
        self.download_ressource(ressource, destination)
        info(f"Ressource récupérée : {destination}")

    def read_jobs(self):
        with open(DOWNLOAD_FILE, "r") as file:
            content = file.read().strip()
        allJobs = content.split("\n")
        self.jobsToDo = [job for job in allJobs
                         if job and job not in self.jobsDone]
        if len(self.jobsToDo) == 0:
            # all jobs are done, write empty file
            with open(DOWNLOAD_FILE, "w") as file:
                file.write("")
            return False
        return True

    def run(self):
        self.user, self.password = [self.coursesData[key] for key in CREDENTIALS]
        self.download_destination = Path(self.coursesData['download_destination'])
        info("Connection à moodle")
        self.login()
        while self.read_jobs():
            info(f"{len(self.jobsToDo)} ressources à télécharger")
            for job in self.jobsToDo:
                jobList = job.split(JOB_SEPARATOR)
                courseUrl, ressourceUrl, destination = [*jobList, *[""]*(3 - len(jobList))]
                if courseUrl == "":
                    continue
                self.collect_ressource(courseUrl, ressourceUrl, destination)
            self.jobsDone.extend(self.jobsToDo)
        self.session.close()
        info("Terminé")
        print("Done.")


bot = MoodleBot()
bot.run()
