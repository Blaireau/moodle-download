# Moodle Download

Moodle download is a tool to automaticaly download, name and store moodle ressources.

## Installation

Clone the repository using the following command :

```bash
git clone git@framagit.org:Blaireau/moodle-download.git
```

## Usage

### Configuring moodle-download

The first thing to do is to configure moodle download.
Create a copy of the file `template_courses.toml` named `courses.toml` and open it in a text editor.
Complete it by following the instructions it contains.

Create an empty file named `ressources.txt` in the moodle-download folder.
This file is used to hold all moodle resources waiting to be downloaded.
Each line should be in the following format :
```txt
<course url> <ressource url> [path]
```

`<course url>` is the url of the moodle page of the course to which the resource belongs
`<ressource url>` is the url of the specific ressource to download
`<path>]` (optional) indicates where the downloaded ressource should be stored (relative to `baseDir`). 

### Running moodle-download

Run the programm with the following command :
```bash
python3 main.py
```

Moodle-download will then download all moodle ressources contained in the file `ressources.txt`.

To know where each file should be stored, moodle-download performs the following steps :

* The default folder to store ressources is `baseDir` (set in `courses.toml`).
* Then, the course folder is determined using `<course url>`. Moodle-download will search through the listed courses in `courses.toml`; if the `moodle` field of a course matches `<course url>`, the course folder is set to the `dir` field of the matching course.
* If `<path>` is specified, it is append to previously computed path. If path ends with a file name, that name is used for the ressource. Otherwise, the original name is kept.
* If `<path>` correspond to one of the special folder name listed in `autoname` (in `courses.toml`), the name of the ressource is automatically set to be `<path>_n.ext` where `n` is an integer. This integer is stored in a hidden file and is updated so that the first ressource you download that way is named `<path>_1.ext` and the following are named `<path>_2.ext`, `<path>_3.ext` and so on.

:warning: Currently, moodle-download only support log in to `moodle.epfl.ch` to download `.pdf` ressources and
log in to `tube.switch.ch` to download courses videos.

## License

This software is free software under GPLv3.

